package scripts.red_ClayMasterV2;
 
import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import javax.swing.JButton;
 
 
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import org.tribot.api.General;
import scripts.red_ClayMasterV2.data.Values;
import sun.java2d.loops.FillRect;


public class GUI {
		private Values values;
        private JFrame frame;
        JCheckBox chckbxUseGe;
        /**
         * Create the application.
         */
        public GUI(Values values) {
        		this.values = values;
        		initialize();
        }

        /**
         * Initialize the contents of the frame.
         */
        private void initialize() {
                frame = new JFrame();
                frame.setBounds(100, 100, 360, 520);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frame.getContentPane().setLayout(null);
               
                JLabel txtRedClayMaster = new JLabel("Red ClayMaster");
                txtRedClayMaster.setFont(new Font("Arial", Font.BOLD, 30));
                txtRedClayMaster.setHorizontalAlignment(SwingConstants.CENTER);
                txtRedClayMaster.setBounds(-2, 0, 359, 50);
                frame.getContentPane().add(txtRedClayMaster);
               
                JLabel txtMouseSpeed = new JLabel("Mouse Speed");
                txtMouseSpeed.setHorizontalAlignment(SwingConstants.CENTER);
                txtMouseSpeed.setFont(new Font("Arial", Font.PLAIN, 20));
                txtMouseSpeed.setBounds(-2, 70, 359, 20);
                frame.getContentPane().add(txtMouseSpeed);
               
                JSlider sliderMouseSpeed = new JSlider();
                sliderMouseSpeed.setPaintLabels(true);
                sliderMouseSpeed.setValue(100);
                sliderMouseSpeed.setPaintTicks(true);
                sliderMouseSpeed.setMajorTickSpacing(50);
                sliderMouseSpeed.setMaximum(300);
                sliderMouseSpeed.setMinimum(50);
                sliderMouseSpeed.setMinorTickSpacing(10);
                sliderMouseSpeed.setBounds(12, 90, 330, 60);
                frame.getContentPane().add(sliderMouseSpeed);
                
                JComboBox<String> locations = new JComboBox<String>();
                locations.setModel(new DefaultComboBoxModel<String>(new String[] {"Edgeville", "More Comming soon"}));
                locations.setSelectedIndex(0);
                locations.setBounds(12, 163, 330, 22);
                frame.getContentPane().add(locations);
               
                JCheckBox chckbxFillBuckets = new JCheckBox("Fill Buckets");
                chckbxFillBuckets.setSelected(true);
                chckbxFillBuckets.setBounds(40, 216, 113, 25);
                frame.getContentPane().add(chckbxFillBuckets);
              
                JCheckBox chckbxMakeSoftClay = new JCheckBox("Make Soft Clay"); 
                chckbxMakeSoftClay.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent arg0) {
                		//chckbxUseGe.setEnabled(chckbxMakeSoftClay.isSelected());
                	}
                });
                           
                chckbxMakeSoftClay.setSelected(true);
                chckbxMakeSoftClay.setBounds(40, 246, 113, 25);
                frame.getContentPane().add(chckbxMakeSoftClay);
                
                chckbxUseGe = new JCheckBox("Use GE(BETA)");
                chckbxUseGe.setBounds(40, 276, 113, 25);
                frame.getContentPane().add(chckbxUseGe);
                chckbxUseGe.setEnabled(false);//GE still in development
               
                JCheckBox chckbxAdvancedPaint = new JCheckBox("Advanced Paint");
                chckbxAdvancedPaint.setToolTipText("Wil be added soon");
                chckbxAdvancedPaint.setEnabled(false);
                chckbxAdvancedPaint.setBounds(40, 306, 145, 25);
                frame.getContentPane().add(chckbxAdvancedPaint);
                
                JCheckBox chckbxWorldHop = new JCheckBox("World Hop");
                chckbxWorldHop.setSelected(false);
                chckbxWorldHop.setBounds(208, 217, 97, 23);
                frame.getContentPane().add(chckbxWorldHop);
                //chckbxWorldHop.setEnabled(false);
                
                JCheckBox chckbxMembersWorlds = new JCheckBox("Members Worlds");
                chckbxMembersWorlds.setBounds(208, 247, 134, 23);
                frame.getContentPane().add(chckbxMembersWorlds);
                
                JCheckBox chckbxDmWorlds = new JCheckBox("DM Worlds");
                chckbxDmWorlds.setBounds(208, 277, 97, 23);
                frame.getContentPane().add(chckbxDmWorlds);


                
                chckbxWorldHop.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent arg0) {
                		chckbxMembersWorlds.setEnabled(chckbxWorldHop.isSelected());
                		chckbxDmWorlds.setEnabled(chckbxWorldHop.isSelected());
                	}
                });
                             
                JSlider worldHopTimeSlider = new JSlider();
                worldHopTimeSlider.setBounds(208, 330, 136, 44);
                worldHopTimeSlider.setPaintLabels(true);
                worldHopTimeSlider.setValue(30);
                worldHopTimeSlider.setPaintTicks(true);
                worldHopTimeSlider.setMajorTickSpacing(60);
                worldHopTimeSlider.setMaximum(180);
                worldHopTimeSlider.setMinimum(0);
                worldHopTimeSlider.setMinorTickSpacing(30);
                frame.getContentPane().add(worldHopTimeSlider);
                
                JSlider worldHopTimeVariantSlider = new JSlider();
                worldHopTimeVariantSlider.setBounds(208, 394, 136, 45);
                worldHopTimeVariantSlider.setPaintLabels(true);
                worldHopTimeVariantSlider.setValue(25);
                worldHopTimeVariantSlider.setPaintTicks(true);
                worldHopTimeVariantSlider.setMajorTickSpacing(25);
                worldHopTimeVariantSlider.setMaximum(100);
                worldHopTimeVariantSlider.setMinimum(0);
                worldHopTimeVariantSlider.setMinorTickSpacing(5);
                frame.getContentPane().add(worldHopTimeVariantSlider);

                if(!chckbxWorldHop.isEnabled()){
                        chckbxMembersWorlds.setEnabled(false);
                        chckbxDmWorlds.setEnabled(false);
                        worldHopTimeSlider.setEnabled(false);
                        worldHopTimeVariantSlider.setEnabled(false);

                }
                
                JButton btnStart = new JButton("Start");
                btnStart.setBounds(70, 450, 97, 25);
                frame.getContentPane().add(btnStart);
                btnStart.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent e) {
                            values.setMouseSpeed(sliderMouseSpeed.getValue());
                            values.setLocation(locations.getSelectedIndex());
                            values.setFillBuckets(chckbxFillBuckets.isSelected());
                            values.setMakeSoftClay(chckbxMakeSoftClay.isSelected());             
                            values.setUseGE(chckbxUseGe.isSelected());
                            values.setWorldHopOn(chckbxWorldHop.isSelected());
                            values.setWorldHopPTP(chckbxMembersWorlds.isSelected());
                            values.setWorldHopDM(chckbxDmWorlds.isSelected());
                            
                            values.setWorldHopTimeIntervals(worldHopTimeSlider.getValue()*60000);//since 1 min = 60,000Milliseconds :)
                            values.setWorldHopTimeVariant(worldHopTimeVariantSlider.getValue());
                            
                            values.setGUI(false);
                           
                            
                            
                            if(chckbxFillBuckets.isSelected()){
                            	values.setStage("FILLING_BUCKETS");
                            }else if(chckbxMakeSoftClay.isSelected()){
                            	values.setStage("MAKING_SOFT_CLAY");
                            }else{
                            	values.setStage("GRAND_EXCHANGE");
                            }
                                           
                            frame.setVisible(false);
                           
                    }
                });
                
               
                JButton btnCancel = new JButton("Cancel");
                btnCancel.setBounds(190, 450, 97, 25);
                frame.getContentPane().add(btnCancel);
                
                JLabel lblTimeBetweenHops = new JLabel("Time Between Hops(min)");
                lblTimeBetweenHops.setBounds(208, 310, 136, 14);
                frame.getContentPane().add(lblTimeBetweenHops);
                
                JLabel lblTimeVariant = new JLabel("Time Variant(%)");
                lblTimeVariant.setBounds(208, 376, 136, 14);
                frame.getContentPane().add(lblTimeVariant);
                
                btnCancel.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                            values.setGUI(false);
                            frame.setVisible(false);
                    }
                });           
        }
 
        public void setVisible(boolean b) {
                frame.setVisible(b);
               
        }
}