package scripts.red_ClayMasterV2.data;
/*
 * This class is still not being used
 */
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public class GeWalk {
	private final static RSArea pathPart0 = new RSArea(new RSTile(3109, 3506), 2);
	private final static RSArea pathPart1 = new RSArea(new RSTile(3121, 3511), 2);
	private final static RSArea pathPart2 = new RSArea(new RSTile(3131, 3517), 2);
	private final static RSArea pathPart3 = new RSArea(new RSTile(3134, 3504), 2);
	private final static RSArea pathPart4 = new RSArea(new RSTile(3134, 3492), 2);
	private final static RSArea pathPart5 = new RSArea(new RSTile(3132, 3481), 1);//reduces it so it doesnt click inside the building
	private final static RSArea pathPart6 = new RSArea(new RSTile(3137, 3465), 1);
	private final static RSArea pathPart7 = new RSArea(new RSTile(3153, 3462), 2);
	private final static RSArea geEntarnce = new RSArea(new RSTile(3165, 3467), 2);
	
	public final static RSArea [] gePath = {(pathPart0), (pathPart1),(pathPart2),(pathPart3),(pathPart4),(pathPart5),(pathPart6),(pathPart7),(geEntarnce)};
}
