package scripts.red_ClayMasterV2.data;
/**
 * @author Red_Spark Created 13/12/2015 Used for storing constants/final
 *         variables that are used trough out the script
 */

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public class Constants {

	// START OF IDs
	private static String emptyBucket = "Bucket";
	private static String waterBucket = "Bucket of water";
	private static String clay = "Clay";
	private static String softClay = "Soft clay";
	private static String waterWell = "Well";
	private static String banker = "Banker";
	// END OF IDs
	
	//START OF ItemPresets
	public final static Item[] FILLING_BUCKETS_ITEMS = { new Item(emptyBucket, 1925, 28, false, false) };
	public final static Item[] MAKING_SOFT_CLAY_ITEMS = { (new Item(waterBucket, 1929, 14, false, false)),
			(new Item(clay, 434, 14, false, false)) };
	public final static Item[] GRAND_EXCHANGE_ITEMS = {};
	//END OF ItemPresets
	
	//START OF RSAreas
	public final static RSArea[] EDGEVILLE_AREA = {};
	public final static RSArea[] EDGEVILLE_BANK_AREA = {
			(new RSArea(new RSTile(3098, 3494, 0), new RSTile(3092, 3498, 0))),
			(new RSArea(new RSTile(3094, 3493, 0), new RSTile(3092, 3488, 0))) };
	public final static RSArea[] EDGEVILLE_WATER_SOURCE_AREA = {
			new RSArea(new RSTile(3086, 3500, 0), new RSTile(3083, 3505, 0)) };
	public final static RSArea[] GE_AREA = { new RSArea(new RSTile(3168, 3485), new RSTile(3161, 3493)) };
	//END OF RSAreas
	
	
	public static Location getWorkLocations() {
		return Location.EDGEVILLE;
	}

	public static Item[] getStageItems(String stage) {//Gets item presets for the stage
		switch (stage) {
		case ("FILLING_BUCKETS"):
			return FILLING_BUCKETS_ITEMS;
		case ("MAKING_SOFT_CLAY"):
			return MAKING_SOFT_CLAY_ITEMS;
		case ("GRAND_EXCHANGE"):
			return GRAND_EXCHANGE_ITEMS;

		}
		return null;
	}

	// START OF ID GETERS
	public static String emptyBucketName() {
		return emptyBucket;
	}

	public static String waterWell() {
		return waterWell;
	}

	public static String waterBucketName() {
		return waterBucket;
	}

	public static String clayName() {
		return clay;
	}

	public static String softClayName() {
		return softClay;
	}

	public static String bankerName() {
		return banker;
	}
	// END OF ID GETERS
	
	
}
