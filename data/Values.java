package scripts.red_ClayMasterV2.data;

import org.tribot.api.General;
import org.tribot.api2007.WorldHopper;

/**
 * @author Red_Spark
 * Created 13/12/2015
 * Used for storing variables thats are used trough out the script
 */
public class Values {

	private boolean DEBUG = false;
	private boolean run = true;
	private String status;//what are we doing now
	private String stage = "";//what stage of the script we are at(example:fillingBuckets, getting supplys from ge and so on)
	private boolean getNewTasks = true;
	private int failSafe = 0; //if the number gets too high that means we are stuck in a loop
	private int softClayCount=0;
	private boolean needToBank = true;
	private boolean worldHop = false;
	private long nextWorldHopTime;
	
	//GUI
	private int mouseSpeed = 100;
	private int location;
	private boolean fillBuckets;
	private	boolean makeSoftClay;
	private boolean useGE;
	private boolean guiOn = true;
	
	private boolean worldHopOn;
	private boolean worldHopPTP;
	private boolean worldHopDM;
	private  long worldHopTimeIntervals;
	private int worldHopTimeVariant;
	


	
	public void setNextHopTime(long runTime){
		long randomPercentage = General.random(0, worldHopTimeVariant);
		int randomNum = General.random(0, 1);//0 = we subtract; 1 = we add
		if(randomNum == 0){
			nextWorldHopTime = worldHopTimeIntervals - (worldHopTimeIntervals/100 * randomPercentage)+(runTime);
			
		}else{
			nextWorldHopTime = worldHopTimeIntervals + (worldHopTimeIntervals/100 * randomPercentage)+(runTime);
		}
		General.println("Next Worl Hop is in:"+(nextWorldHopTime-runTime)/60000+" minutes");//changing the print out into minutes from milliseconds
			
	}
	
	public int getWorldToHop(){
		int world = WorldHopper.getRandomWorld(worldHopPTP, worldHopDM);
		General.println("World will be hoping to:"+ world);
		return world;
	}
	public boolean worldHop(){		
		return worldHopOn && worldHop;
	}
	public boolean timeToHop(long runTime){
		if(nextWorldHopTime < runTime){
			this.worldHop = true;
			if(DEBUG)
				General.println("runTime:"+runTime+"   nextWorldHopTime:"+nextWorldHopTime);
			return true;
		}
		return false;
	}
	
	public void setWorldHop(boolean b){
		this.worldHop = b;
	}
	public void setWorldHopOn(boolean b){
		this.worldHopOn = b;
	}
	public boolean isWorldHopOn(){
		return worldHopOn;
	}
	public void setWorldHopPTP(boolean b){
		this.worldHopPTP = b;
	}
	public void setWorldHopDM(boolean b){
		this.worldHopDM = b;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public boolean getNewTasks() {
		return getNewTasks;
	}

	public void setGetNewTasks(boolean getNewTasks) {
		this.getNewTasks = getNewTasks;
	}
	public void addFail(){
		this.failSafe++;
	}
	public void resetFailCount(){
		this.failSafe = 0;
	}
	public boolean areWeStuck(){
		if(failSafe > 10)//TODO 10 be too high
			return true;
		return false;
	}
	public void setRun(boolean b){
		run = b;
	}
	public boolean run(){
		return run;
	}
	public void addSoftClay(int count){
		softClayCount+=count;
	}
	public int getSoftClayCount(){
		return softClayCount;
	}
	public void setMouseSpeed(int mouseSpeed){
		this.mouseSpeed = mouseSpeed;
	}
	public int getMouseSpeed(){
		return mouseSpeed;
	}
	public void setLocation(int locationIndex){
		this.location = locationIndex;
	}
	public int getLocation(){
		return this.location;
	}
	public void setFillBuckets(boolean b){
		this.fillBuckets = b;
	}
	public boolean fillBuckets(){
		return fillBuckets;
	}
	public void setMakeSoftClay(boolean b){
		this.makeSoftClay = b;
	}
	public boolean makeSoftClay(){
		return makeSoftClay;
	}
	public void setUseGE(boolean b){
		this.useGE = b;
	}
	public boolean useGE(){
		return useGE;
	}
	public void setGUI(boolean b){
		this.guiOn = b;
	}
	public boolean guiOn(){
		return guiOn;
	}
	public boolean needToBank(){
		return needToBank;
	}
	public void needToBank(Boolean b){
		needToBank = b;
	}
	public long getWorldHopTimeIntervals() {
		return worldHopTimeIntervals;
	}
	public void setWorldHopTimeIntervals(int worldHopTimeIntervals) {
		this.worldHopTimeIntervals = worldHopTimeIntervals;
	}
	public int getWorldHopTimeVariant() {
		return worldHopTimeVariant;
	}
	public void setWorldHopTimeVariant(int worldHopTimeVariant) {
		this.worldHopTimeVariant = worldHopTimeVariant;
	}




	
}
