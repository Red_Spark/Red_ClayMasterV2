package scripts.red_ClayMasterV2.data;

import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

public enum Location {
	EDGEVILLE(Constants.EDGEVILLE_AREA, Constants.EDGEVILLE_BANK_AREA, Constants.EDGEVILLE_WATER_SOURCE_AREA);
	
	private final RSArea[] AREA;
	private final RSArea[] BANK_AREA;
	private final RSArea[] WATER_SOURCE_AREA;
	
	private Location(RSArea[] area, RSArea[] bankArea, RSArea[] waterSourceArea){
		this.AREA = area;
		this.BANK_AREA = bankArea;
		this.WATER_SOURCE_AREA = waterSourceArea;
	}
	
	public boolean inArea(RSTile playerPosition){
		for(int i = 0; i < AREA.length; i++){
			if(AREA[i].contains(playerPosition))
				return true;	
		}
		return false;
	}
	public boolean inBankArea(RSTile playerPosition){
		for(int i = 0; i < BANK_AREA.length; i++){
			if(BANK_AREA[i].contains(playerPosition)){
				return true;	
			}
		}
		return false;
	}
	public boolean inWaterArea(RSTile playerPosition){
		for(int i = 0; i < this.WATER_SOURCE_AREA.length; i++){
			if(this.WATER_SOURCE_AREA[i].contains(playerPosition))
				return true;	
		}
		return false;
	}
	public static boolean inGeArea() {
		return (Constants.GE_AREA[0].contains(Player.getPosition()));
	}
	public boolean closeToWaterSource(int distance){
		RSObject[] waterSources = Objects.findNearest(distance, Constants.waterWell());
		if(waterSources.length > 0 && waterSources[0] != null)
			return true;
		return false;
	}
	public boolean waterSourceOnScreen(){
		RSObject[] waterSources = Objects.findNearest(10, Constants.waterWell());
		if(waterSources.length > 0 && waterSources[0] != null){
			if(waterSources[0].isOnScreen()){
				return true;
			}
		}
		return false;
		
	}
	public boolean closeToBank(){
		RSObject[] banker = Objects.findNearest(5, Constants.bankerName());
		if(banker.length > 0 && banker[0] != null)
			return true;
		return false;
	}
	public RSArea[] getWaterSourceArea(){
		return WATER_SOURCE_AREA;
	}
	public RSTile getRandomWaterSourceTile(){
		//TODO FIX THIS 
		return WATER_SOURCE_AREA[0].getRandomTile();
	}
	public RSTile getRandomBankTile(){
		//TODO FIX THIS
		return BANK_AREA[0].getRandomTile();
	}
	
	
	
}
	
