
package scripts.red_ClayMasterV2.data;
//Still not sure if ill need it(not used)
import scripts.red_ClayMasterV2.tasks.Task;

public class TaskInfo {
	private String name;
	private Task task;
	public TaskInfo(String name, Task task){
		this.name = name;
		this.task = task;
	}
	public Task returnTask(){
		return task;
	}
	public boolean isThisTask(String name){
		if(name.equalsIgnoreCase(this.name)){
			return true;
		}
		return false;
	}
}