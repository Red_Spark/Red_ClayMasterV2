package scripts.red_ClayMasterV2.data;

/**
 * @author Red_Spark Created 13/12/2015 Stores and items and values tied to it
 */
public class Item {
	private String name;
	private int id;
	private int ammount;
	private boolean noted;
	private boolean stackable;

	public Item(String name, int id, int ammount, boolean noted, boolean stackable) {
		this.name = name;
		this.id = id;
		this.ammount = ammount;
		this.noted = noted;
		this.stackable = stackable;

	}

	public String getName() {
		return this.name;
	}

	public int getId() {
		return this.id;
	}

	public int getAmmount() {
		return this.ammount;
	}

	public boolean noted() {
		return this.noted;
	}

	public boolean stackable() {
		return this.stackable;
	}
}
