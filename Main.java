package scripts.red_ClayMasterV2;
/**
 * @author Red_Spark
 * Created on 13/12/2015
 */


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.text.DecimalFormat;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Login;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;
//import scripts.redUtil.api.GE07.GE_Check_Official;
import scripts.red_ClayMasterV2.data.Values;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;


@ScriptManifest(authors = "Red_Spark", category = "Money Making", name = "Red_ClayMaster V2", version = 2.6)

public class Main extends Script implements Painting, Ending{
	
	final ScriptManifest MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class);
	public final String TITLE = MANIFEST.name()+" "+MANIFEST.version()+" by Red_Spark";
	private final boolean DEBUG = false;
	private Values values = new Values();
	private TaskManager manager;

	public void onStart(){
		General.println("----------------------------------------------------------------");
		GUI gui = new GUI(values);
        gui.setVisible(true);
        while(values.guiOn()){//Waiting for the user to input all GUI settings
                sleep(500);
        }  
        General.println("Mouse Speed set to:"+values.getMouseSpeed());
        Mouse.setSpeed(values.getMouseSpeed());
        
		
		while(Camera.getCameraAngle() != 100)
			Camera.setCameraAngle(100);
		General.println("----------------------------------------------------------------");
		if(values.isWorldHopOn())
			values.setNextHopTime(timeRan);		
	}
	
	
	public void run() {
		onStart();
		loop(20,50);
	}
	
	private void loop(int min, int max){
		manager = new TaskManager(values);
		while(values.run()){
			if(values.areWeStuck()){//checks to see if we are stuck on a task
				General.println("We are stuck, reseting tasks!");
				General.println("If you see this message too often please");
				General.println("Contact Red_Spark and \"Print Script Stack Trace\"");
				values.setGetNewTasks(true);
				values.needToBank(true);
			}
			if(values.getNewTasks()){	
				if(DEBUG)
					General.println("Getting new tasks");
				manager.clearTasks();
				manager.setStageTasks(values.getStage());
				if(DEBUG)
					General.println("For stage:"+values.getStage());
				
				values.setGetNewTasks(false);
			}
			Task task = manager.getValidTask();
            if (task != null) {
                values.setStatus(task.status());
                if(DEBUG)
                	General.println(task.status());
                task.execute();
                sleep(min,max);
            }else{
            	if(DEBUG)
            		General.println("Task == null");
            	values.setGetNewTasks(true);
            }
            
            if(values.isWorldHopOn()){
            	if(values.timeToHop(timeRan)){
            		General.println("Will be hoping world soon...");
            		manager.values.setNextHopTime(timeRan);
            	}
            }
            
		}
		//General.println("Script has stoped");
		Login.logout();
	}

	@Override
	public void onEnd() {
		General.println("----------------------------------------------------------------");
		General.println("Stoped after:"+Timing.msToString(timeRan));
		General.println("Made:"+values.getSoftClayCount()+" soft clay");
		//General.println("Profit:"+(SOFT_CLAY_PRICE-CLAY_PRICE)*values.getSoftClayCount());
		General.println("----------------------------------------------------------------");
		
	}
	long timeRan;
	public static final long startTime = System.currentTimeMillis();
    //final int SOFT_CLAY_PRICE = GE_Check_Official.getPrice(1761);
    //final int CLAY_PRICE = GE_Check_Official.getPrice(434);
  
    
	public void onPaint(Graphics g) {

		DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###");
		String hourlyProfitString;
		String totalProfitString;
		String hourlyClayString;
		String totalClayMadeString;
		timeRan = System.currentTimeMillis() - startTime;

		
        g.setFont(new Font("Verdana", Font.BOLD, 20));
        g.setColor(new Color(255, 0, 51));
        g.drawString(TITLE, 10, 40);
       
        g.setFont(new Font("Verdana", Font.BOLD, 14));
        g.drawString("Time Running:"+ Timing.msToString(timeRan), 10, 60); 
        
        totalClayMadeString = df.format(values.getSoftClayCount());
        g.drawString("Clay Made:"+totalClayMadeString, 10, 80);
        
        hourlyClayString = df.format(((long)values.getSoftClayCount() * 3600000 / timeRan));
        g.drawString("Clay Made P/H:"+hourlyClayString, 10, 100);
        
        //hourlyProfitString = df.format((long)values.getSoftClayCount() * 3600000 / timeRan * (SOFT_CLAY_PRICE-CLAY_PRICE));
        //g.drawString("Profit P/H:"+hourlyProfitString, 10, 120);
        
        //totalProfitString = df.format((SOFT_CLAY_PRICE-CLAY_PRICE)*values.getSoftClayCount());
        //g.drawString("Total Profit:"+totalProfitString, 10, 140);
       
        g.drawString("Current ACTION:"+values.getStatus(), 10, 160);
        
 
	}
	
	


}
