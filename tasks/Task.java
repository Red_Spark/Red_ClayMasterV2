package scripts.red_ClayMasterV2.tasks;


/**
 * 
 * @author Red_Spark
 * Created 13/12/2015
 * Task framework
 *
 */

public abstract class Task {
    public abstract void execute();

    public abstract boolean validate();

    public abstract String status();
}
