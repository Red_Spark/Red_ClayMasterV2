package scripts.red_ClayMasterV2.tasks.navigation;
import org.tribot.api.General;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that walks to water source
 */
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSTile;

import scripts.redUtil.api.walking07.Walking07;
import scripts.red_ClayMasterV2.data.Constants;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;

public class WalkToWaterSource extends Task{
	private final boolean DEBUG = false;
	private TaskManager manager;
	public WalkToWaterSource(TaskManager manager){
		this.manager = manager;
	}

	public void execute() {
		if(DEBUG)
			General.println("WalkToWaterSource:Validate:True");
		if(walkToWater()){
			manager.removeTask(this);
			manager.values.resetFailCount();
		}
	}

	public boolean validate() {
		manager.values.addFail();
		return Inventory.getCount(Constants.emptyBucketName()) > 0 && !Constants.getWorkLocations().waterSourceOnScreen();
	}

	public int priority() {
		return 5;
	}

	public String status() {
		return "Walking To Water Source";
	}
	private boolean walkToWater(){
		RSTile randomTile = Constants.getWorkLocations().getRandomWaterSourceTile();
		if(randomTile == null)
			return false;
		
		RSTile[] path = Walking.generateStraightPath(randomTile);
		if(path.length == 0)
			return false;
		return Walking07.walkPath(path, 2, new Condition(){
			public boolean active() {
				General.sleep(100, 300);
				return Constants.getWorkLocations().closeToWaterSource(7);
			}
		});
	}

}
