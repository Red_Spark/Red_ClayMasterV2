package scripts.red_ClayMasterV2.tasks.navigation;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that walks to the GE
 */

import org.tribot.api.General;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSTile;
import scripts.redUtil.api.walking07.Walking07;
import scripts.red_ClayMasterV2.data.GeWalk;
import scripts.red_ClayMasterV2.data.Location;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;

public class WalkToGE extends Task {
	private final boolean DEBUG = true;
	private TaskManager manager;
	public WalkToGE(TaskManager manager){
		this.manager = manager;
	}
	@Override
	public void execute() {
		walkToGE();
		
	}

	@Override
	public boolean validate() {
		return manager.values.getStage() == "GRAND_EXCHANGE" && !Location.inGeArea();
	}



	@Override
	public String status() {
		// TODO Auto-generated method stub
		return "Walking to GE";
	}
	private boolean walkToGE(){//TODO Make a fail safe
		RSTile[] path;
		for(int i = 0; i< GeWalk.gePath.length; i++){
			path = Walking.generateStraightPath(GeWalk.gePath[i].getRandomTile());
			if(path.length > 0){
				if(!Walking07.walkPath(path, 1)){
					if(DEBUG)
						General.println("Walk failed 1");
					
					i--;
				}
			}else{
				if(DEBUG)
					General.println("Walk failed 2");
				i--;
			}
		}
		return true;
	}

}
