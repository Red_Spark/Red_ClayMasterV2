package scripts.red_ClayMasterV2.tasks.navigation;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that walks form water source to the bank
 */

import org.tribot.api.General;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSTile;
import scripts.redUtil.api.walking07.Walking07;
import scripts.red_ClayMasterV2.data.Constants;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;

public class WalkToBank extends Task {
	private final boolean DEBUG = false;
	private TaskManager manager;
	public WalkToBank(TaskManager manager){
		this.manager = manager;
	}
	public void execute() {
		if(walkToBank()){
			manager.clearTasks();
			manager.values.resetFailCount();
			
		}
		
		
	}

	public boolean validate() {
		manager.values.addFail();
		if(manager.values.getStage() == "MAKING_SOFT_CLAY" && !Constants.getWorkLocations().inBankArea(Player.getPosition())){
			if(DEBUG)
				General.println("Walking back to bank");
			return true;		
		}
		
		return !Constants.getWorkLocations().inBankArea(Player.getPosition()) && Inventory.getCount(Constants.emptyBucketName()) == 0;
	}
	
	public String status() {
		return "Walking to bank";
	}
	private boolean walkToBank(){
		RSTile randomTile = Constants.getWorkLocations().getRandomBankTile();
		if(randomTile == null)
			return false;
		RSTile[] path = Walking.generateStraightPath(randomTile);
		if(path.length == 0)
			return false;
		return Walking07.walkPath(path, 3, new Condition(){
			public boolean active() {
				return Constants.getWorkLocations().inBankArea(Player.getPosition());
				
			}
			
		});
	}

}
