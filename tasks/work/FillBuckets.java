package scripts.red_ClayMasterV2.tasks.work;
import org.tribot.api.DynamicClicking;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that fills empty buckets with water from  water source
 */
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Game;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;

import scripts.redUtil.other.DynamicWait;
import scripts.redUtil.antiBan.AntiBan;
import scripts.red_ClayMasterV2.data.Constants;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;
import scripts.red_ClayMasterV2.tasks.navigation.WalkToWaterSource;

public class FillBuckets extends Task{
	private final boolean DEBUG = false;
	private TaskManager manager;
	public FillBuckets(TaskManager manager){
		this.manager = manager;
	}

	public void execute() {
		if(DEBUG)
			General.println("FillBuckets:Validate:True");
		if(fillBuckets()){
			manager.values.needToBank(true);
			manager.removeTask(this);
			manager.values.resetFailCount();
		}
		
	}

	public boolean validate() {
		manager.values.addFail();
		return Constants.getWorkLocations().closeToWaterSource(7) && Inventory.getCount(Constants.emptyBucketName()) > 0;
	}

	public String status() {
		return "Filling Buckets";
	}
	private boolean fillBuckets(){
		RSObject[] well = Objects.findNearest(20, Constants.waterWell());//Stores well
        RSItem[] emptyBuckets = Inventory.find(Constants.emptyBucketName());//Finds empty buckets in the inventory
        String upText = Game.getUptext();//Stores up text
        
        if(emptyBuckets.length <=0 || emptyBuckets[0] == null){
        	//We have no buckets, we need to go get some
        	General.println("No buckets in inventory");
        	//lets reset our tasks
        	manager.values.setGetNewTasks(true);
        	return false;
        }
        if(well.length <= 0 || well[0] == null){
        	//No well found
        	General.println("No well nearby");
        	manager.addTasks(new WalkToWaterSource(manager));//Add a task to walk to it
        }
      //Click on empty bucket
        if(DEBUG)
        	General.println("upText:"+ upText);
        if(upText == null){
        	return false;
        }
        if (!upText.equalsIgnoreCase("Use Bucket ->")) {
        	if(DEBUG){
        		General.println("Clicking on bucket");
        	}
        	if(!emptyBuckets[0].click("Use"))
        		return false;  
        	AntiBan.waitItemInteractionDelay();
        	if(!DynamicWait.waitForUpText("Use Bucket ->", 3000)){
        		if(DEBUG)
        			General.println("FillBuckets:waitForUpText failed");
        		//return false;
        	}
        }
        upText = Game.getUptext();
        if(DEBUG)
        	General.println("upText:"+ upText);
        if (upText != null && upText.equalsIgnoreCase("Use Bucket ->")) {
        	//if(!well[0].click("Use Bucket -> Well")){
        	if(DEBUG)
        		General.println("Clicking on well");
        	DynamicClicking.clickRSObject(well[0], "Use Bucket -> Well");
        	if(Timing.waitCrosshair(500) != 2){
        		if(DEBUG){
        			General.println("Failed to click on well with bucket");
        		}
        		return false;
        	}
        	
    		AntiBan.waitItemInteractionDelay();
    		AntiBan.waitUntilIdle(500, 1000);
    		int waterBucketCount = Inventory.getCount(Constants.waterBucketName());
    		int waterBucketCount2;
    		long t = System.currentTimeMillis();
    		if(DEBUG)
    			General.println("FillingBuckets:Going into while loop");
    		while(true){
                AntiBan.moveCamera();
                AntiBan.leaveGame();
                AntiBan.mouseMovement();
                AntiBan.rightClick();
                
                if(Inventory.getCount(Constants.emptyBucketName()) == 0){
                	if(DEBUG)
                		General.println("All buckets full");
                	return true;//filled all buckets
                }
                if(System.currentTimeMillis() - t > 2000){
                	waterBucketCount2 = Inventory.getCount(Constants.waterBucketName());
                	if(waterBucketCount == waterBucketCount2){
                		General.println("Not filling buckets.Task Reset");
                		return false;//buckets not being filled
                	}
                	t = System.currentTimeMillis();//everything is ok, so we reset the counter
                	waterBucketCount = waterBucketCount2;
                }
                General.sleep(20, 60);
                
    		}
        }  
        return false;
	}

}
