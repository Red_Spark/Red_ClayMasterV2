package scripts.red_ClayMasterV2.tasks.work;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that makes soft clay by using water bucket on clay
 */
import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Game;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSItem;

import scripts.redUtil.other.DynamicWait;
import scripts.redUtil.antiBan.AntiBan;
import scripts.red_ClayMasterV2.data.Constants;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;

public class MakeSoftClay extends Task{
	private final boolean DEBUG = false;
	private TaskManager manager;
	public MakeSoftClay(TaskManager manager){
		this.manager = manager;
	}
	public void execute() {
		if(Banking.isBankScreenOpen()){
			Banking.close();
			if(DynamicWait.waitForBankClose(2000)){//waits for the bank to close
				if(DEBUG)
					General.println("MakeSoftClay.class:Bank has been closed");
			}
		}else if(makeSoftClay()){
			manager.values.needToBank(true);
			manager.clearTasks();
			manager.values.resetFailCount();
		}
		
		
	}
	public boolean validate() {
		manager.values.addFail();
		return Inventory.getCount(Constants.clayName()) > 0 && Inventory.getCount(Constants.waterBucketName()) > 0 && Constants.getWorkLocations().inBankArea(Player.getPosition());
	}

	public String status() {
		return "Making Soft Clay";
	}
	private boolean makeSoftClay(){
		RSItem[] waterBuckets = Inventory.find(Constants.waterBucketName());//Finds water buckets in inventory
        RSItem[] clay = Inventory.find(Constants.clayName());//Find clay in inventory
        String upText = Game.getUptext();//stores up text
        
        if(waterBuckets.length <= 0 || waterBuckets[0] == null){
        	//We have no water buckets, we need to go get some
        	General.println("No water buckets in inventory");
        	//lets reset our tasks
        	manager.values.setGetNewTasks(true);
        	return false;
        }
        if(clay.length <= 0 || clay[0] == null){
        	//We have no clay, we need to go get some
        	General.println("No clay in inventory");
        	//lets reset our tasks
        	manager.values.setGetNewTasks(true);
        	return false;
        }
        if (upText == null){
        	return false;
        }
        if (!upText.equalsIgnoreCase("Use Bucket of water ->")) {
        	if(!waterBuckets[waterBuckets.length -1].click())
        		return false;  
        	AntiBan.waitItemInteractionDelay();
        	if(!DynamicWait.waitForUpText("Use Bucket of water ->", 3000))
        		return false;
        upText = Game.getUptext();	
        }
        if (upText != null && upText.equalsIgnoreCase("Use Bucket of water ->")) {
        	if(!clay[0].click("Use Bucket of water -> Clay"))
        		return false;
        	int waterBucketCount = Inventory.getCount(Constants.waterBucketName());
    		AntiBan.waitItemInteractionDelay();
    		RSInterfaceChild clayInterface;
    		for(int i = 0; i < 30; i++){//TODO improvised sleep methods, need to change this
				clayInterface = Interfaces.get(270, 13);
				if(clayInterface != null && !clayInterface.isHidden()){
                    clayInterface.click();
                    AntiBan.waitItemInteractionDelay();
                    if(DynamicWait.waitForItem(Constants.softClayName(), 0, 4000));{
                    	if(DEBUG)
                    		General.println("DynamicWait in interfaceLoop returned false");
                    }
                    
                    break;
				}
				General.sleep(100);
			}
    		long t = System.currentTimeMillis();
    		while(true){ 

                AntiBan.moveCamera();
                AntiBan.leaveGame();
                AntiBan.mouseMovement();
                AntiBan.rightClick();
                if(DEBUG)
                	General.println(Inventory.getCount(Constants.waterBucketName()));
                if(Inventory.getCount(Constants.waterBucketName()) == 0){
                	//General.println("Used all water buckets");
                	return true;
                }
                if(Inventory.getCount(Constants.clayName()) == 0){
                	//General.println("Used all clay");
                	return true;
                }
                if(System.currentTimeMillis() - t > 2000){
                	int waterBucketCount2 = Inventory.getCount(Constants.waterBucketName());
                	if(waterBucketCount == waterBucketCount2){
                		General.println("Soft Clay is not being made");
                		return false;
                	}
                	t = System.currentTimeMillis();//everything is ok, so we reset the counter
                	waterBucketCount = waterBucketCount2;//also reset the buckets
                }
                General.sleep(20, 60);
    		}
        }
		return false;
	}

}
