package scripts.red_ClayMasterV2.tasks;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;

import org.tribot.api2007.WorldHopper;
import scripts.redUtil.other.DynamicWait;


public class WorldHoper extends Task {
	private final boolean DEBUG = false;
	private TaskManager manager;
	int lastWorld  = -1;
	public WorldHoper(TaskManager manager){
		this.manager = manager;
	}

	public void execute() {
		// TODO Auto-generated method stub
		if(Banking.isBankScreenOpen()){
			Banking.close();
			if(DynamicWait.waitForBankClose(2000)){//waits for the bank to close
				if(DEBUG)
					General.println("WorldHoper.class:Bank has been closed");
			}else{
				if(DEBUG)
					General.println("WorldHoper.class:Bank Closing failed");
				return;//breaks out of the task so it can try again :)
			}
		}
		if(lastWorld != -1 && lastWorld != WorldHopper.getWorld()){

		}

		lastWorld = WorldHopper.getWorld();
		General.println("World hop successful:"+WorldHopper.changeWorld(manager.values.getWorldToHop()));
		manager.values.setWorldHop(false);
		//Waits for the world to change
		Timing.waitCondition(() -> {
			int cWorld = WorldHopper.getWorld();
			General.println("Last World" + lastWorld + ": Current world:"+cWorld);
			General.sleep(200, 600);
			return lastWorld !=  cWorld;
		},General.random(4000,7000));


		
	}

	public boolean validate() {
		if(DEBUG)
			General.println("WorldHop Validate:"+manager.values.worldHop());
		return manager.values.worldHop();
	}

	public String status() {
		// TODO Auto-generated method stub
		return "Hoping to diffrent world";
	}

}
