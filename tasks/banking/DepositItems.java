package scripts.red_ClayMasterV2.tasks.banking;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that deposits items to the bank
 */

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;

import scripts.redUtil.other.DynamicWait;
import scripts.red_ClayMasterV2.data.Constants;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;
public class DepositItems extends Task{
	private final boolean DEBUG = false;
	private TaskManager manager;
	

	public DepositItems(TaskManager manager) {
		this.manager = manager;
	}

	public void execute() {
	
		
		switch(manager.values.getStage()){
		case("FILLING_BUCKETS"):
			if(!depositAll()){
				General.println("Failed to depositAll for:"+manager.values.getStage());
				//will try to figure out why we failed
				if(!Banking.isBankScreenOpen())//check if the bank screen got closed
					manager.clearTasks();
					//manager.addTasks(new OpenBank(manager));//if yes add task to open it
				break;
			}
			manager.removeTask(this);//since it was successful we remove this task from the task list
			manager.values.resetFailCount();
			break;
		case("MAKING_SOFT_CLAY"):
			if(!depositAll()){
				General.println("Failed to depositAll for:"+manager.values.getStage());
				//will try to figure out why we failed
				if(!Banking.isBankScreenOpen())//check if the bank screen got closed
					manager.clearTasks();
					//manager.addTasks(new OpenBank(manager));//if yes add task to open it
				break;
			}
			manager.removeTask(this);//since it was successful we remove this task from the task list
			manager.values.resetFailCount();
			break;
			
		
		default:
			General.println("DepositItems->execute->switch failed");
			General.println("Report this to Red_Spark and \"Print Script Stack Trace\"");
			break;
		}
			
		
	}


	public boolean validate() {
		manager.values.addFail();
		switch(manager.values.getStage()){
		case("FILLING_BUCKETS"):
			if(DEBUG){//debug code
				General.println("DepositItems validate1:"+(Banking.isBankScreenOpen() && (Inventory.getCount(Constants.waterBucketName()) > 0 || Inventory.getCount(Constants.softClayName()) > 0 
						|| Inventory.getCount(Constants.clayName()) > 0 )));
			
				General.println("Banking.isBankScreenOpen():"+Banking.isBankScreenOpen());
				General.println("Inventory.getCount(Constants.waterBucketName()) > 0:"+ (Inventory.getCount(Constants.waterBucketName()) > 0));
				General.println("Inventory.getCount(Constants.softClayName()) > 0:"+ (Inventory.getCount(Constants.softClayName()) > 0));
				General.println("Inventory.getCount(Constants.clayName()) > 0 ):"+(Inventory.getCount(Constants.clayName()) > 0 ));
				General.println("||:" + (Inventory.getCount(Constants.waterBucketName()) > 0 || Inventory.getCount(Constants.softClayName()) > 0 
								|| Inventory.getCount(Constants.clayName()) > 0 ));
			
			
			
			
			}
			return  Banking.isBankScreenOpen() && (Inventory.getCount(Constants.waterBucketName()) > 0 || Inventory.getCount(Constants.softClayName()) > 0 
					|| Inventory.getCount(Constants.clayName()) > 0 );//TODO make this neater 
		case("MAKING_SOFT_CLAY"):
			if(DEBUG){//debug code
				General.println("DepositItems validate2:"+ (Banking.isBankScreenOpen() && (Inventory.getCount(Constants.waterBucketName()) <= 0 || Inventory.getCount(Constants.clayName()) <= 0)));
				General.println("Banking.isBankScreenOpen():"+ Banking.isBankScreenOpen());
				General.println("Inventory.getCount(Constants.waterBucketName()) <= 0:"+ (Inventory.getCount(Constants.waterBucketName()) <= 0));
				General.println("Inventory.getCount(Constants.clayName()) <= 0):"+(Inventory.getCount(Constants.clayName()) <= 0));
				General.println("||"+(Inventory.getCount(Constants.waterBucketName()) <= 0 || Inventory.getCount(Constants.clayName()) <= 0));
			
			
			}
			return Banking.isBankScreenOpen() && (Inventory.getCount(Constants.waterBucketName()) <= 0 || Inventory.getCount(Constants.clayName()) <= 0);
		}
		return false;

	}
	public String status() {
		return "Depositing Items";
	}
	private boolean depositAll(){
		int i = Inventory.getCount(Constants.softClayName());
		if(Inventory.getAll().length > 0){
			Banking.depositAll();
			if(!DynamicWait.waitForDepositAll(General.random(2000, 4000))){
				i = 0;
                return false;//items failed to deposit, return false
			}
		}
		manager.values.addSoftClay(i);
		return true;
	}

}
