package scripts.red_ClayMasterV2.tasks.banking;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that withdraws items from the bank
 */
import org.tribot.api.General;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that withdraws items from the bank
 */
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;

import scripts.redUtil.other.DynamicWait;
import scripts.redUtil.api.banking07.Banking07;
import scripts.red_ClayMasterV2.data.Constants;
import scripts.red_ClayMasterV2.data.Item;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;

public class WithdrawItems extends Task{
	private final boolean DEBUG = false;
	private TaskManager manager;
	public WithdrawItems(TaskManager manager){
		this.manager = manager;
	}
	//START OF MAIN METHODS
	public void execute() {
		if(withdrawItems()){
			manager.values.needToBank(false);
			manager.removeTask(this);
			manager.values.resetFailCount();
		}else{
			
		}
		
	}
	public boolean validate() {
		manager.values.addFail();
		return Banking.isBankScreenOpen() && !Inventory.isFull();
	}
	
	public String status() {
		return "Withdrawing Items";
	}
	//END OF MAIN METHODS
	private boolean withdrawItems(){
		Item[] items = Constants.getStageItems(manager.values.getStage());
		RSItem[] item;
		for(int i = 0; i < items.length; i++){
			DynamicWait.waitForBankOpen(3000);
			item = Banking.find(items[i].getName());
			
			if(item.length == 0 || item[0] == null){
				General.println("Cannot find "+items[i].getName()+" in bank");
				DynamicWait.waitForBankOpen(3000);//this check if items aree loaded
				item = Banking.find(items[i].getName());//FIXME	might be redundant code, was put in because of a item retrieving error(no longer happening, but i left it in here just in case)
				if(item.length <= 0 || item[0] == null){//Re-check it, sometime the bot is too fast
					General.println("Re-check for item:"+items[i].getName()+" failed too");
					General.println("Report this to Red_Spark and \"Print Script Stack Trace\"");
					return false;
				}
				
			}
			int itemCount = item[0].getStack();
			if(DEBUG)
				General.println(items[i].getName() +":"+ itemCount +" in bank");
			if(Inventory.isFull()){
				General.println("Inventory is full, we overdrw something");
				manager.clearTasks();
				manager.addTasks(new DepositItems(manager));
				return false;//can't withdraw any more items
			}
			if(itemCount>items[i].getAmmount()){
				if(!Banking07.withdrawItem(item[0], items[i].getAmmount(), items[i].noted(), items[i].stackable(), General.random(6000, 8000))){
					General.println("Failed to withdraw item with all/specific ammount");
					return false;
				}
			}else if(itemCount>1){
				if(!Banking07.withdrawItem(item[0], -1, items[i].noted(), items[i].stackable(), General.random(6000, 8000))){//withdraws all-but one bucket
					General.println("Failed to withdraw with all-but one");
					return false;
				}
			}else{
				General.println("No more:"+items[i].getName()+" in bank");
				String itemName = items[i].getName();
				if(Constants.waterBucketName().equalsIgnoreCase(itemName)){
					if(!manager.setStageTasks("FILLING_BUCKETS"))
						manager.values.setRun(false);
				}else if(Constants.emptyBucketName().equalsIgnoreCase(itemName)){
					if(!manager.setStageTasks("MAKING_SOFT_CLAY"))
						manager.values.setRun(false);
					
				}else if(Constants.clayName().equalsIgnoreCase(itemName)){
					//TODO
					General.println("no more clay");
					manager.values.setRun(false);
				}
				return false;
				
			}
			General.sleep(20, 60);
		}
			
			
		return true;

	}
}
