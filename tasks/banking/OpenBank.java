package scripts.red_ClayMasterV2.tasks.banking;
/**
 * @author Red_Spark
 * Created 13/12/2015
 * Task that open the bank
 */


import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Player;
import scripts.red_ClayMasterV2.data.Constants;
import scripts.red_ClayMasterV2.tasks.Task;
import scripts.red_ClayMasterV2.tasks.TaskManager;

public class OpenBank extends Task {
	//private final boolean GLOBAL_DEBUG = false;
	private TaskManager manager;
	public OpenBank(TaskManager manager){
		this.manager = manager;
	}

	public void execute() {
		if(Banking.openBank()){
			Timing.waitCondition(new Condition() {

				public boolean active() {
					General.sleep(20, 60);
					return Banking.isBankScreenOpen();
				}
				
			}, General.random(2000, 3000));	
			manager.removeTask(this);
			manager.values.resetFailCount();
		}
		
	}

	public boolean validate() {
		manager.values.addFail();
		return !Banking.isBankScreenOpen() && Constants.getWorkLocations().inBankArea(Player.getPosition()) && manager.values.needToBank();
	}
	
	public String status() {
		
		return "Opening Bank";
	}

}
