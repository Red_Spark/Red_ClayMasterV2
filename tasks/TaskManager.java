package scripts.red_ClayMasterV2.tasks;
/**
 * Created on 13/12/2015
 * Got the idea for this framework from Andrews script Netamis_Orb_Charger
 * Stores/sorts tasks
 */
import java.util.ArrayList;
import java.util.List;

import org.tribot.api.General;

import scripts.red_ClayMasterV2.data.Values;
import scripts.red_ClayMasterV2.tasks.banking.DepositItems;
import scripts.red_ClayMasterV2.tasks.banking.OpenBank;
import scripts.red_ClayMasterV2.tasks.banking.WithdrawItems;
import scripts.red_ClayMasterV2.tasks.navigation.WalkToBank;
import scripts.red_ClayMasterV2.tasks.navigation.WalkToGE;
import scripts.red_ClayMasterV2.tasks.navigation.WalkToWaterSource;
import scripts.red_ClayMasterV2.tasks.work.FillBuckets;
import scripts.red_ClayMasterV2.tasks.work.MakeSoftClay;


public class TaskManager {
	private final boolean DEBUG = false;
	public TaskManager(Values values) {
        list = new ArrayList<>();
        this.values = values;
    }	
	private List<Task> list;
	public Values values;
	//TASK PRE-SETS
    public final Task[] FILLING_BUCKETS_TASKS = {new WorldHoper(this), new OpenBank(this), new DepositItems(this), new WithdrawItems(this),
    												new WalkToWaterSource(this), new FillBuckets(this), new WalkToBank(this)};
	public final Task[] MAKING_SOFT_CLAY_TASKS = {new WorldHoper(this), new OpenBank(this), new DepositItems(this), new WithdrawItems(this), new MakeSoftClay(this), new WalkToBank(this)};
	public final Task[] GRAND_EXCHANGE_TASKS = {new WalkToGE(this)};
	//END OF TASK PRE-SETS
	
	//MAIN METHOS
    public void addTasks(Task... tasks) {
        for (Task task: tasks) {
            if (!list.contains(task)) {
            	if(DEBUG)
            		General.println("Task added");
                list.add(task);
            }
        }
    }

    public void removeTask(Task task) {
        if (list.contains(task)) {
        	if(DEBUG)
        		General.println("Task removes");
            list.remove(task);
        }
    }

    public void clearTasks() {
    	if(DEBUG)
    		General.println("Task cleared");
        list.clear();
    }

    public int size() {
        return list.size();
    }

    /*
    Return the highest priority valid task.
     */
    public Task getValidTask() {
        if (list.size() > 0) {
        	for(int i = 0; i < list.size(); i++){
        		if(list.get(i).validate()){
        			return list.get(i);
        		}  
        		General.sleep(20, 40);
        	}        
        }
        return null;
    }
    /*
    Overridden compare method from the Comparator interface used by the
    Collections sort method to determine what task is at the head of the priority.
    Added to the standard compare method is a check to see if each
    task validates or not before comparing them.
    If one task does not validate and the other does, the task that validates
    assumes higher priority.
    Refer to the javadoc for the Comparator interface for an explanation of the
    return values from this method.
     */
    
    //END OF MAIN METHOS
    public boolean setStageTasks(String stage){
		switch(stage){
		case(""):	
			if(DEBUG)
				General.println("getOrganized tryint to set a stage");
			return getOrganized();
		case("FILLING_BUCKETS"):
			if(values.fillBuckets()){
				values.setStage("FILLING_BUCKETS");
				if(DEBUG)
					General.println("Stage Set to 1");
				clearTasks();
				addTasks(FILLING_BUCKETS_TASKS);
				return true;
			}
			General.println("Nothing else to do 1");
			values.setRun(false);
			return false;
		
		case("MAKING_SOFT_CLAY"):
			if(values.makeSoftClay()){
				values.setStage("MAKING_SOFT_CLAY");
				if(DEBUG)
					General.println("Stage Set to 2");
				clearTasks();
				addTasks(MAKING_SOFT_CLAY_TASKS);
				return true;
			}
			General.println("Nothing else to do 2");
			values.setRun(false);
			return false;
		case("GRAND_EXCHANGE"):
			if(DEBUG)
				General.println("values.useGE():"+values.useGE());
			if(values.useGE()){
				values.setStage("GRAND_EXCHANGE");
				if(DEBUG)
					General.println("Stage Set to 3");
				clearTasks();
				addTasks(GRAND_EXCHANGE_TASKS);
				return true;
			}
			General.println("Nothing else to do 3");
			values.setRun(false);
			return false;
		default:
			General.println("Invalid Stage Input");
			General.println("Report this to Red_Spark and \"Print Script Stack Trace\"");
			return false;
		}
	}
	//TODO
	private boolean getOrganized(){
		if(values.fillBuckets()){
			values.setStage("FILLING_BUCKETS");
			if(DEBUG)
				General.println("Stage Set to 1 by getOrganized");
			clearTasks();
			addTasks(FILLING_BUCKETS_TASKS);
			return true;
		}else if(values.makeSoftClay()){
			values.setStage("MAKING_SOFT_CLAY");
			if(DEBUG)
				General.println("Stage Set to 2 by getOrganized");
			clearTasks();
			addTasks(MAKING_SOFT_CLAY_TASKS);
			return true;
		}else if(values.useGE()){
			values.setStage("GRAND_EXCHANGE");
			if(DEBUG)
				General.println("Stage Set to 3 by getOrganized");
			clearTasks();
			addTasks(GRAND_EXCHANGE_TASKS);
			return true;
		}
		values.setRun(false);
		General.println("getOrginized failed");
		return false;
	}

}
